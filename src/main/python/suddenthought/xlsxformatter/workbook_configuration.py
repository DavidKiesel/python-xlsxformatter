#   -*- coding: utf-8 -*-

##############################################################################
# copyrights and license
#
# Copyright (c) 2021 David Harris Kiesel
#
# Licensed under the MIT License. See LICENSE in the project root for license
# information.
##############################################################################

# docstring conventions;
# https://www.python.org/dev/peps/pep-0257/
# https://www.sphinx-doc.org/en/master/usage/restructuredtext/domains.html#the-python-domain
"""
A module for formatting xlsx.
"""

from copy import \
    copy
from io import \
    BytesIO

# https://pypi.org/project/XlsxWriter/
# https://xlsxwriter.readthedocs.io/
# https://github.com/jmcnamara/XlsxWriter
import xlsxwriter


XLST_MAX_ROW_OFFSET = 1048575


class WorkbookConfiguration:
    # docstring conventions;
    # https://www.python.org/dev/peps/pep-0257/
    # https://www.sphinx-doc.org/en/master/usage/restructuredtext/domains.html#the-python-domain
    """
    Workbook configuration.
    """

    @staticmethod
    def from_json(
        configuration_json
    ):
        # docstring conventions;
        # https://www.python.org/dev/peps/pep-0257/
        # https://www.sphinx-doc.org/en/master/usage/restructuredtext/domains.html#the-python-domain
        """
        Create workbook configuration from JSON.

        :param configuration_json: Workbook configuration as JSON.
        :type configuration_json: dict

        :return: Workbook configuration.
        :rtype: WorkbookConfiguration
        """

        workbook_configuration = \
            WorkbookConfiguration()

        workbook_configuration.workbook_options = \
            configuration_json.get(
                'workbook_options',
                {}
            )

        workbook_configuration.cell_format_settings = \
            configuration_json.get(
                'cell_format_settings',
                {}
            )

        workbook_configuration.default_worksheet_settings = \
            configuration_json.get(
                'worksheet_settings',
                {}
            )

        workbook_configuration.default_column_settings = \
            configuration_json.get(
                'column_settings',
                []
            )

        workbook_configuration.default_row_settings = \
            configuration_json.get(
                'row_settings',
                []
            )

        workbook_configuration.worksheets = \
            configuration_json.get(
                'worksheets',
                []
            )

        return workbook_configuration

    def __init__(
        self
    ):
        # docstring conventions;
        # https://www.python.org/dev/peps/pep-0257/
        # https://www.sphinx-doc.org/en/master/usage/restructuredtext/domains.html#the-python-domain
        """
        Constructor.
        """

        self.workbook_options = None

        self.cell_format_settings = None

        self.default_worksheet_settings = None

        self.default_column_settings = None

        self.default_row_settings = None

        self.worksheets = []

    def apply(
        self,
        workbook=None,
        number_of_worksheets=1,
        max_row_offset=XLST_MAX_ROW_OFFSET
    ):
        # docstring conventions;
        # https://www.python.org/dev/peps/pep-0257/
        # https://www.sphinx-doc.org/en/master/usage/restructuredtext/domains.html#the-python-domain
        """
        Apply workbook configuration to a workbook.

        :param workbook: Workbook; if ``None``, creates workbook; defaults to ``None``.
        :type workbook: xlsxwriter.Workbook, optional

        :param number_of_worksheets: Number of worksheets; defaults to ``1``.
        :type number_of_worksheets: int, optional

        :param max_row_offset: Maximum worksheet row offset; defaults to ``XLST_MAX_ROW_OFFSET``.
        :type max_row_offset: int or list, optional

        :return: Workbook.
        :rtype: xlsxwriter.Workbook
        """

        workbook_options = \
            self.workbook_options

        cell_format_settings = \
            self.cell_format_settings

        default_worksheet_settings = \
            self.default_worksheet_settings

        default_column_settings = \
            self.default_column_settings

        default_row_settings = \
            self.default_row_settings

        worksheet_configurations = \
            self.worksheets

        if workbook is None:
            output = BytesIO()

            workbook = \
                xlsxwriter.Workbook(
                    output,
                    options=workbook_options
                )

        cell_formats = {}

        for cell_format_name in cell_format_settings:
            cell_formats[cell_format_name] = \
                workbook.add_format(
                    cell_format_settings[cell_format_name]
                )

        len_worksheet_configurations = \
            len(worksheet_configurations)

        for worksheet_offset in range(0, number_of_worksheets):
            worksheet_ordinal = worksheet_offset + 1

            if worksheet_offset < len_worksheet_configurations:
                worksheet_configuration = \
                    worksheet_configurations[
                        worksheet_offset
                    ]

                if worksheet_configuration is None:
                    worksheet_configuration = {}
            else:
                worksheet_configuration = {}

            worksheet_name = \
                worksheet_configuration.get(
                    'name',
                    'Sheet{}'.format(
                        worksheet_ordinal
                    )
                )

            worksheet_settings = \
                worksheet_configuration.get(
                    'worksheet_settings',
                    default_worksheet_settings
                )

            column_settings = \
                worksheet_configuration.get(
                    'column_settings',
                    default_column_settings
                )

            row_settings = \
                worksheet_configuration.get(
                    'row_settings',
                    default_row_settings
                )

            if isinstance(max_row_offset, int):
                worksheet_max_row_offset = \
                    max_row_offset
            else:
                worksheet_max_row_offset = \
                    max_row_offset[worksheet_offset]

            worksheet = \
                workbook.add_worksheet(
                    worksheet_name
                )

            for column_setting in column_settings:
                worksheet.set_column(
                    column_setting.get('first_col'),
                    column_setting.get('last_col'),
                    width=column_setting.get('width'),
                    cell_format=cell_formats.get(
                        column_setting.get('cell_format')
                    ),
                    options=column_setting.get('options')
                )

            for row_setting in row_settings:
                worksheet.set_row(
                    row_setting.get('row'),
                    cell_format=cell_formats.get(
                        row_setting.get('cell_format')
                    )
                )

            autofilter = \
                copy(
                    worksheet_settings.get('autofilter')
                )

            if autofilter is not None:
                if isinstance(autofilter, list):
                    if autofilter[2] < 0:
                        autofilter[2] = \
                            XLST_MAX_ROW_OFFSET + autofilter[2] + 1

                    if autofilter[2] > worksheet_max_row_offset:
                        autofilter[2] = worksheet_max_row_offset

                    worksheet.autofilter(
                        *autofilter
                    )
                else:
                    worksheet.autofilter(
                        autofilter
                    )

            freeze_panes = \
                worksheet_settings.get('freeze_panes')

            if freeze_panes is not None:
                if isinstance(freeze_panes, list):
                    worksheet.freeze_panes(
                        *freeze_panes
                    )
                else:
                    worksheet.freeze_panes(
                        freeze_panes
                    )

        return workbook
