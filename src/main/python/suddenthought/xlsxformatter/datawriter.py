#   -*- coding: utf-8 -*-

##############################################################################
# copyrights and license
#
# Copyright (c) 2021 David Harris Kiesel
#
# Licensed under the MIT License. See LICENSE in the project root for license
# information.
##############################################################################

# docstring conventions;
# https://www.python.org/dev/peps/pep-0257/
# https://www.sphinx-doc.org/en/master/usage/restructuredtext/domains.html#the-python-domain
"""
A module for writing data.
"""


def write(
    workbook,
    workbook_rows
):
    # docstring conventions;
    # https://www.python.org/dev/peps/pep-0257/
    # https://www.sphinx-doc.org/en/master/usage/restructuredtext/domains.html#the-python-domain
    """
    Write rows to workbook worksheets.

    :param workbook: Workbook.
    :type workbook: xlsxwriter.Workbook

    :param workbook_rows: List of list of rows; one element per worksheet.
    :type workbook_rows: list
    """

    for worksheet_offset, worksheet in enumerate(workbook.worksheets()):
        write_worksheet(
            workbook,
            worksheet_offset,
            workbook_rows[worksheet_offset]
        )


def write_worksheet(
    workbook,
    worksheet_offset,
    rows
):
    # docstring conventions;
    # https://www.python.org/dev/peps/pep-0257/
    # https://www.sphinx-doc.org/en/master/usage/restructuredtext/domains.html#the-python-domain
    """
    Write rows to a workbook worksheet.

    :param workbook: Workbook.
    :type workbook: xlsxwriter.Workbook

    :param worksheet_offset: Workbook worksheet offset.
    :type worksheet_offset: int

    :param rows: List of rows; one element per row.
    :type rows: list
    """

    worksheet = \
        workbook.worksheets()[worksheet_offset]

    text_column_offsets = set()

    for column_offset in worksheet.col_formats:
        num_format = \
            worksheet.col_formats[
                column_offset
            ].num_format

        if num_format == '@':
            text_column_offsets.add(
                column_offset
            )

    for row_offset, row in enumerate(rows):
        for column_offset, cell_value in enumerate(row):
            if cell_value[0:1] == '=':
                if column_offset in worksheet.col_formats:
                    cell_format = \
                        worksheet.col_formats[
                            column_offset
                        ]

                    if cell_format.num_format == '@':
                        cell_format = \
                            workbook.formats[2]
                else:
                    cell_format = None

                worksheet.write_formula(
                    row_offset,
                    column_offset,
                    cell_value,
                    cell_format,
                    ''
                )
            elif column_offset in text_column_offsets:
                worksheet.write_string(
                    row_offset,
                    column_offset,
                    cell_value
                )
            else:
                worksheet.write(
                    row_offset,
                    column_offset,
                    cell_value
                )
