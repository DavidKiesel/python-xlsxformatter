#   -*- coding: utf-8 -*-

##############################################################################
# copyrights and license
#
# Copyright (c) 2021 David Harris Kiesel
#
# Licensed under the MIT License. See LICENSE in the project root for license
# information.
##############################################################################

# docstring conventions;
# https://www.python.org/dev/peps/pep-0257/
# https://www.sphinx-doc.org/en/master/usage/restructuredtext/domains.html#the-python-domain
"""
A module for reading data.
"""

import csv


def read(
    reader
):
    # docstring conventions;
    # https://www.python.org/dev/peps/pep-0257/
    # https://www.sphinx-doc.org/en/master/usage/restructuredtext/domains.html#the-python-domain
    """
    Read from row and column reader; return rows.

    :param reader: Row and column reader; e.g., :py:class:`_csv.reader`.
    :type reader: reader object

    :return: Rows; list of lists of cell values.
    :rtype: list
    """

    rows = []

    for row_offset, row in enumerate(reader):
        columns = []
        for column_offset, cell_value in enumerate(row):
            columns.append(cell_value)

        rows.append(columns)

    return rows


def read_csv(
    csv_fd
):
    # docstring conventions;
    # https://www.python.org/dev/peps/pep-0257/
    # https://www.sphinx-doc.org/en/master/usage/restructuredtext/domains.html#the-python-domain
    """
    Read CSV file object; return rows.

    :param csv_fd: CSV file object.
    :type csv_fd: file object

    :return: Rows; list of lists of cell values.
    :rtype: list
    """
    csv_reader = \
        csv.reader(
            csv_fd
        )

    return read(csv_reader)
