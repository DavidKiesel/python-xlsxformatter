#   -*- coding: utf-8 -*-

##############################################################################
# copyrights and license
#
# Copyright (c) 2021 David Harris Kiesel
#
# Licensed under the MIT License. See LICENSE in the project root for license
# information.
##############################################################################

"""
A module for handling command-line interface functionality.
"""

import argparse
import io
import json
import sys

# from this distribution package
from suddenthought.xlsxformatter.workbook_configuration import \
    WorkbookConfiguration
from suddenthought.xlsxformatter.datareader import \
    read_csv
from suddenthought.xlsxformatter.datawriter import \
    write


def get_binary_writer(
    file_path
):
    # docstring conventions;
    # https://www.python.org/dev/peps/pep-0257/
    # https://www.sphinx-doc.org/en/master/usage/restructuredtext/domains.html#the-python-domain
    """
    Get binary writer.

    Create instance of :py:class:`argparse.FileType` with parameter ``mode``
    set to ``'wb'``.

    Get ``writer`` by calling instance of ``argparse.FileType`` with parameter
    ``file_path``.

    If ``writer`` is instance of :py:class:`io.TextIOWrapper` (``'-'``
    sys.stdout case), then return ``writer.buffer``, which will be an instance
    of :py:class:`io.BufferedWriter`.

    Otherwise, return ``writer``, which will be an instance of
    :py:class:`io.BufferedWriter`.

    :param file_path: File path.
    :type file_path: str

    :return: A binary writer.
    :rtype: io.BufferedWriter
    """

    writer = \
        argparse.FileType(
            mode='wb'
        )(
            file_path
        )

    if isinstance(
        writer,
        io.TextIOWrapper
    ):
        writer = \
            writer.buffer

    return writer


def get_parser():
    # docstring conventions;
    # https://www.python.org/dev/peps/pep-0257/
    # https://www.sphinx-doc.org/en/master/usage/restructuredtext/domains.html#the-python-domain
    """
    Get parser.

    :return: A parser.
    :rtype: argparse.ArgumentParser
    """

    parser = \
        argparse.ArgumentParser(
            prog='xlsxf',
            formatter_class=argparse.RawDescriptionHelpFormatter,
            description="""
Office Open XML Workbook formatter.
""",
            add_help=True,
            epilog="""
WORKBOOK CONFIGURATION FORMAT:

{
    "workbook_options": workbook options,
    "cell_format_settings": workbook format settings,
    "worksheet_settings": workbook default worksheet settings,
    "column_settings": workbook default worksheet column settings,
    "row_settings": workbook default worksheet row settings,
    "worksheets": various worksheet-by-worksheet settings
}

workbook options:
    - dict
    - key: XlsxWriter Workbook constructor option name
    - value: option value
    - see https://xlsxwriter.readthedocs.io/workbook.html#Workbook

workbook format settings:
    - dict
    - key: a user-chosen name for a collection of XlsxWriter Workbook
      add_format properties
    - value: dict that is a collection of XlsxWriter Workbook add_format
      properties
    - see https://xlsxwriter.readthedocs.io/workbook.html#add_format

workbook default worksheet settings:
    - used for each worksheet where worksheet settings not specified in
      "worksheets"
    - dict
    - key: a supported XlsxWriter Worksheet setting
    - value: appropriate value
    - see Worksheet methods at
      https://xlsxwriter.readthedocs.io/worksheet.html
    - supported settings:
        - autofilter:
          https://xlsxwriter.readthedocs.io/worksheet.html#autofilter
        - freeze_panes:
          https://xlsxwriter.readthedocs.io/worksheet.html#freeze_panes

workbook default column settings:
    - used for each worksheet where worksheet column settings not specified in
      "worksheets"
    - list of dict
    - key: Worksheet.set_column() parameter keyword
    - value: Worksheet.set_column() parameter keyword value
        - cell_format value if given must be one of the names given in
          "cell_format_settings"
    - see https://xlsxwriter.readthedocs.io/worksheet.html#set_column

workbook default row settings:
    - used for each worksheet where worksheet row settings not specified in
      "worksheets"
    - list of dict
    - key: Worksheet.set_row() parameter keyword
    - value: Worksheet.set_row() parameter keyword value
        - cell_format value if given must be one of the names given in
          "cell_format_settings"
    - see https://xlsxwriter.readthedocs.io/worksheet.html#set_row

various worksheet-by-worksheet settings:
    - if a worksheet-by-worksheet setting key is provided, then its value
      overrides the workbook default for worksheets
    - list of dict
    - if an element is null or not provided, it is treated like {}
    - key:
        - name
        - worksheet_settings
        - column settings
        - row_settings
    - value: an appropriate value

WORKBOOK CONFIGURATION EXAMPLES:

A simple example with just workbook defaults set.

{
    "workbook_options": {
        "strings_to_numbers": true
    },
    "cell_format_settings": {
        "general": {
            "text_wrap": true,
            "valign": "top",
            "num_format": "General"
        },
        "header": {
            "text_wrap": true,
            "valign": "bottom",
            "bold": true
        },
        "text": {
            "text_wrap": true,
            "valign": "top",
            "num_format": "@"
        },
        "number_p0": {
            "text_wrap": true,
            "valign": "top",
            "align": "right",
            "num_format": "0"
        },
        "datetime": {
            "text_wrap": true,
            "valign": "top",
            "align": "right",
            "num_format": "yyyy-mm-dd HH:MM:SS.000"
        }
    },
    "worksheet_settings": {
        "autofilter": [
            0,
            0,
            -1,
            3
        ],
        "freeze_panes": [
            1,
            0
        ]
    },
    "column_settings": [
        {
            "first_col": 0,
            "last_col": 0,
            "width": 40,
            "cell_format": "text"
        },
        {
            "first_col": 1,
            "last_col": 1,
            "width": 40,
            "cell_format": "text",
            "options": {
                "hidden": true
            }
        },
        {
            "first_col": 2,
            "last_col": 2,
            "width": 20,
            "cell_format": "number_p0"
        },
        {
            "first_col": 3,
            "last_col": 3,
            "width": 30,
            "cell_format": "datetime"
        }
    ],
    "row_settings": [
        {
            "row": 0,
            "cell_format": "header"
        }
    ]
}

A more complicated example with a worksheet override set.

{
    "workbook_options": {
        "strings_to_numbers": true
    },
    "cell_format_settings": {
        "general": {
            "text_wrap": true,
            "valign": "top",
            "num_format": "General"
        },
        "header": {
            "text_wrap": true,
            "valign": "bottom",
            "bold": true
        },
        "text": {
            "text_wrap": true,
            "valign": "top",
            "num_format": "@"
        },
        "number_p0": {
            "text_wrap": true,
            "valign": "top",
            "align": "right",
            "num_format": "0"
        },
        "datetime": {
            "text_wrap": true,
            "valign": "top",
            "align": "right",
            "num_format": "yyyy-mm-dd HH:MM:SS.000"
        }
    },
    "worksheet_settings": {
        "autofilter": [
            0,
            0,
            -1,
            3
        ],
        "freeze_panes": [
            1,
            0
        ]
    },
    "column_settings": [
        {
            "first_col": 0,
            "last_col": 0,
            "width": 40,
            "cell_format": "text"
        },
        {
            "first_col": 1,
            "last_col": 1,
            "width": 40,
            "cell_format": "text",
            "options": {
                "hidden": true
            }
        },
        {
            "first_col": 2,
            "last_col": 2,
            "width": 20,
            "cell_format": "number_p0"
        },
        {
            "first_col": 3,
            "last_col": 3,
            "width": 30,
            "cell_format": "datetime"
        }
    ],
    "row_settings": [
        {
            "row": 0,
            "cell_format": "header"
        }
    ],
    "worksheets": [
        null,
        {
            "name": "foo",
            "worksheet_settings": {},
            "column_settings": [
                {
                    "first_col": 0,
                    "last_col": 0,
                    "width": 20,
                    "cell_format": "text"
                },
                {
                    "first_col": 1,
                    "last_col": 1,
                    "width": 20,
                    "cell_format": "text"
                },
                {
                    "first_col": 2,
                    "last_col": 2,
                    "width": 20,
                    "cell_format": "number_p0"
                },
                {
                    "first_col": 3,
                    "last_col": 3,
                    "width": 30,
                    "cell_format": "datetime"
                }
            ]
        }
    ]
}

COMMAND EXAMPLES:

    Read data from data.csv.  Write to standard output.

        %(prog)s \\
            data.csv

    Read data from data.csv.  Read configuration from data.config.json.  Write
    to standard output.

        %(prog)s \\
            --config data.config.json \\
            data.csv

    Read data from data_1.csv and data_2.csv.  Read configuration from
    workbook.config.json.  Write to standard output.

        %(prog)s \\
            --config workbook.config.json \\
            data_1.csv \\
            data_2.csv

    Read data from data_1.csv and data_2.csv.  Read configuration from
    workbook.config.json.  Write to data.xlsx.

        %(prog)s \\
            --config workbook.config.json \\
            --output data.xlsx \\
            data_1.csv \\
            data_2.csv
"""
        )

    # type=argparse.FileType(mode='wb')
    # works for ordinary files but use get_binary_writer
    # to also be able to handle '-' sys.stdout case
    parser.add_argument(
        '--output',
        '-o',
        type=get_binary_writer,
        default='-',
        help="""
output; default: '-' (standard output)
"""
    )

    parser.add_argument(
        '--config',
        '-c',
        type=load_json_from_file,
        default={},
        help="""
file with workbook configuration in JSON format;
default: '{}'
"""
    )

    parser.add_argument(
        'input',
        type=argparse.FileType('r'),
        nargs='*',
        help="""
input
"""
    )

    return parser


def load_json_from_file(
    file_path
):
    # docstring conventions;
    # https://www.python.org/dev/peps/pep-0257/
    # https://www.sphinx-doc.org/en/master/usage/restructuredtext/domains.html#the-python-domain
    """
    Load JSON from file.

    :param file_path: File path.
    :type file_path: str

    :return: Result of :py:class:`json.load` taking argument ``file_path``.
    :rtype: dict or list
    """

    with open(file_path) as file:
        o = \
            json.load(file)

    return o


def main(
    list_of_readers,
    config={},
    output=sys.stdout.buffer
):
    # docstring conventions;
    # https://www.python.org/dev/peps/pep-0257/
    # https://www.sphinx-doc.org/en/master/usage/restructuredtext/domains.html#the-python-domain
    """
    Use ``list_of_readers`` (data) and ``config`` (workbook configuration) to
    generate workbook and send to output.

    :param list_of_readers: List of readers.
    :type list_of_readers: list

    :param config: Workbook configuration; defaults to ``{}``.
    :type config: dict, optional

    :param output: Output; defaults to ``sys.stdout.buffer``.
    :type output: io.BufferedWriter, optional
    """

    workbook_rows = []

    workbook_max_row_offsets = []

    for reader in list_of_readers:
        rows = \
            read_csv(
                reader
            )

        reader.close()

        workbook_rows.append(
            rows
        )

        workbook_max_row_offsets.append(
            len(rows) - 1
        )

    workbook_configuration = \
        WorkbookConfiguration.from_json(
            config
        )

    workbook = \
        workbook_configuration.apply(
            number_of_worksheets=len(workbook_rows),
            max_row_offset=workbook_max_row_offsets
        )

    write(
        workbook,
        workbook_rows
    )

    workbook_file = workbook.filename

    workbook.close()

    xlsx_bytes = workbook_file.getvalue()

    output.write(xlsx_bytes)

    output.close()
