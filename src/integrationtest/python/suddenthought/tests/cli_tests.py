#   -*- coding: utf-8 -*-

##############################################################################
# copyrights and license
#
# Copyright (c) 2021 David Harris Kiesel
#
# Licensed under the MIT License. See LICENSE in the project root for license
# information.
##############################################################################

# docstring conventions;
# https://www.python.org/dev/peps/pep-0257/
# https://www.sphinx-doc.org/en/master/usage/restructuredtext/domains.html#the-python-domain
"""
A module for integration testing of
:py:class:`suddenthought.xlsxformatter.cli`.
"""

from filecmp import \
    dircmp
import logging
import os.path
import tempfile
import unittest
from unittest import \
    TestCase
from zipfile import \
    ZipFile

# local
import suddenthought.xlsxformatter.cli as cli

module_dir = \
    os.path.dirname(
        __file__
    )


def cmpdirs(
    dcmp,
    check_left_only=True,
    check_right_only=True,
    check_diff_files=True,
    check_funny_files=True,
    check_subdirs=True
):
    # docstring conventions;
    # https://www.python.org/dev/peps/pep-0257/
    # https://www.sphinx-doc.org/en/master/usage/restructuredtext/domains.html#the-python-domain
    """
    Compare files.

    :param dcmp: Directory comparison object.
    :type dcmp: filecmp.dircmp

    :param check_left_only: Check ``left_only``; defaults to ``True``.
    :type check_left_only: bool, optional

    :param check_right_only: Check ``right_only``; defaults to ``True``.
    :type check_right_only: bool, optional

    :param check_diff_files: Check ``diff_files``; defaults to ``True``.
    :type check_diff_files: bool, optional

    :param check_funny_files: Check ``funny_files``; defaults to ``True``.
    :type check_funny_files: bool, optional

    :param check_subdirs: Check ``subdirs``; defaults to ``True``.
    :type check_subdirs: bool, optional

    :return: True or False.
    :rtype: bool
    """

    if check_left_only:
        for name in dcmp.left_only:
            logging.error(
                'left_only %s found in %s',
                name,
                dcmp.left
            )

            return False

    if check_right_only:
        for name in dcmp.right_only:
            logging.error(
                'right_only %s found in %s',
                name,
                dcmp.right
            )

            return False

    if check_diff_files:
        for name in dcmp.diff_files:
            logging.error(
                'diff_file %s found in %s and %s',
                name,
                dcmp.left,
                dcmp.right
            )

            return False

    if check_funny_files:
        for name in dcmp.funny_files:
            logging.error(
                'funny_file %s found in %s and %s',
                name,
                dcmp.left,
                dcmp.right
            )

            return False

    if check_subdirs:
        for sub_dcmp in dcmp.subdirs.values():
            result = \
                cmpdirs(sub_dcmp)

            if not result:
                return False

    return True


class Test(TestCase):
    # docstring conventions;
    # https://www.python.org/dev/peps/pep-0257/
    # https://www.sphinx-doc.org/en/master/usage/restructuredtext/domains.html#the-python-domain
    """
    Test.
    """

    def test__normal(self):
        # docstring conventions;
        # https://www.python.org/dev/peps/pep-0257/
        # https://www.sphinx-doc.org/en/master/usage/restructuredtext/domains.html#the-python-domain
        """
        Test normal conditions.
        """

        ######################################################################
        # prepare

        subtests_args = []

        subtests_args.append(
            [
                os.path.join(
                    module_dir,
                    'cli_tests',
                    'test__normal',
                    '{}'.format(len(subtests_args)),
                    'data.csv'
                )
            ]
        )

        subtests_args.append(
            [
                '--config',
                os.path.join(
                    module_dir,
                    'cli_tests',
                    'test__normal',
                    '{}'.format(len(subtests_args)),
                    'data.config.json'
                ),
                os.path.join(
                    module_dir,
                    'cli_tests',
                    'test__normal',
                    '{}'.format(len(subtests_args)),
                    'data.csv'
                )
            ]
        )

        subtests_args.append(
            [
                '--config',
                os.path.join(
                    module_dir,
                    'cli_tests',
                    'test__normal',
                    '{}'.format(len(subtests_args)),
                    'workbook.config.json'
                ),
                os.path.join(
                    module_dir,
                    'cli_tests',
                    'test__normal',
                    '{}'.format(len(subtests_args)),
                    'data_1.csv'
                ),
                os.path.join(
                    module_dir,
                    'cli_tests',
                    'test__normal',
                    '{}'.format(len(subtests_args)),
                    'data_2.csv'
                )
            ]
        )

        ######################################################################
        # test and assert

        for i in range(0, len(subtests_args)):
            with self.subTest(
                i=i
            ):
                args = \
                    cli.get_parser().parse_args(
                        subtests_args[i]
                    )

                with tempfile.TemporaryDirectory() as tmp_dir:
                    output_path = \
                        os.path.join(
                            tmp_dir,
                            'workbook.xlsx'
                        )

                    with open(
                        output_path,
                        'wb'
                    ) as output:
                        cli.main(
                            args.input,
                            config=args.config,
                            output=output
                        )

                        unzip_dir = \
                            os.path.join(
                                tmp_dir,
                                'unzip'
                            )

                        with ZipFile(
                            output_path,
                            'r'
                        ) as zip_fp:
                            zip_fp.extractall(
                                unzip_dir
                            )

                        expected_unzip_dir = \
                            os.path.join(
                                module_dir,
                                'cli_tests',
                                'test__normal',
                                '{}'.format(i),
                                'unzip'
                            )

                        dcmp = \
                            dircmp(
                                expected_unzip_dir,
                                unzip_dir,
                                ignore=[
                                    'core.xml'
                                ]
                            )

                        dcmp.report_full_closure()
                        print('*' * 79)

                        result = \
                            cmpdirs(dcmp)

                        self.assertTrue(
                            result
                        )


if __name__ == '__main__':
    unittest.main()
