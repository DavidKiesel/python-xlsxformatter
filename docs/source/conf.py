# Configuration file for the Sphinx documentation builder.
#
# This file only contains a selection of the most common options. For a full
# list see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Path setup --------------------------------------------------------------

# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.
#
# import os
# import sys
# sys.path.insert(0, os.path.abspath('.'))

import os
import sys

sys.path.insert(
    0,
    os.path.abspath(
        os.path.join(
            '..',
            '..',
            'src',
            'main',
            'python'
        )
    )
)

sys.path.insert(
    0,
    os.path.abspath(
        os.path.join(
            '..',
            '..',
            'src',
            'integrationtest',
            'python'
        )
    )
)

# -- Project information -----------------------------------------------------

project = 'python-xlsxformatter'
copyright = '2021, David Harris Kiesel'
author = 'David Harris Kiesel'

# The full version, including alpha/beta/rc tags
release = '1.0.0.dev'


# -- General configuration ---------------------------------------------------

# Add any Sphinx extension module names here, as strings. They can be
# extensions coming with Sphinx (named 'sphinx.ext.*') or your custom
# ones.
extensions = [
    'sphinx.ext.autodoc',           # Grabs documentation from inside modules
    'sphinx.ext.autosectionlabel',  # Allow reference sections using its title
    'sphinx.ext.intersphinx'        # Link to other projects' documentation
]

# Add any paths that contain templates here, relative to this directory.
templates_path = ['_templates']

# List of patterns, relative to source directory, that match files and
# directories to ignore when looking for source files.
# This pattern also affects html_static_path and html_extra_path.
exclude_patterns = []


# -- Options for HTML output -------------------------------------------------

# The theme to use for HTML and HTML Help pages.  See the documentation for
# a list of builtin themes.
#
html_theme = 'alabaster'

# Add any paths that contain custom static files (such as style sheets) here,
# relative to this directory. They are copied after the builtin static files,
# so a file named "default.css" will overwrite the builtin "default.css".
html_static_path = ['_static']

# -- Options for autodoc -----------------------------------------------------

# https://www.sphinx-doc.org/en/master/usage/extensions/autodoc.html#confval-autodoc_default_options
autodoc_default_options = \
    {
        'special-members': '__init__'
    }

# -- Options for intersphinx -------------------------------------------------

# https://www.sphinx-doc.org/en/master/usage/extensions/intersphinx.html#configuration
# note that the following entry does not work
#        'xlsxwriter': (
#            'https://xlsxwriter.readthedocs.io',
#            None
#        )
intersphinx_mapping = \
    {
        'python': (
            'https://docs.python.org/3',
            None
        )
    }
