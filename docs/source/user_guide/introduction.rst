Introduction
============

Apply xlsx formatting.

Uses Python package `XlsxWriter <https://pypi.org/project/XlsxWriter>`_.
