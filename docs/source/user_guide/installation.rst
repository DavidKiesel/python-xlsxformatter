Installation
============

Installation with ``pip``
-------------------------

To install using the standard ``pip`` utility, execute the command below.

.. code-block:: bash

   pip \
       install \
       suddenthought.xlsxformatter

Installation with ``pipx``
--------------------------

To install using the ``pipx`` utility and the default ``pyenv`` Python version,
execute the command below.

.. code-block:: bash

   PIPX_DEFAULT_PYTHON="$(pyenv root)/versions/$(pyenv global)/bin/python" \
   pipx \
       install \
       suddenthought.xlsxformatter
