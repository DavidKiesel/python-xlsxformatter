Usage
=====

.. code-block:: bash

   $ xlsf --help
   usage: xlsxf [-h] [--output OUTPUT] [--config CONFIG] [input [input ...]]
   
   Office Open XML Workbook formatter.
   
   positional arguments:
     input                 input
   
   optional arguments:
     -h, --help            show this help message and exit
     --output OUTPUT, -o OUTPUT
                           output; default: '-' (standard output)
     --config CONFIG, -c CONFIG
                           file with workbook configuration in JSON format;
                           default: '{}'
   
   WORKBOOK CONFIGURATION FORMAT:
   
   {
       "workbook_options": workbook options,
       "cell_format_settings": workbook format settings,
       "worksheet_settings": workbook default worksheet settings,
       "column_settings": workbook default worksheet column settings,
       "row_settings": workbook default worksheet row settings,
       "worksheets": various worksheet-by-worksheet settings
   }
   
   workbook options:
       - dict
       - key: XlsxWriter Workbook constructor option name
       - value: option value
       - see https://xlsxwriter.readthedocs.io/workbook.html#Workbook
   
   workbook format settings:
       - dict
       - key: a user-chosen name for a collection of XlsxWriter Workbook
         add_format properties
       - value: dict that is a collection of XlsxWriter Workbook add_format
         properties
       - see https://xlsxwriter.readthedocs.io/workbook.html#add_format
   
   workbook default worksheet settings:
       - used for each worksheet where worksheet settings not specified in
         "worksheets"
       - dict
       - key: a supported XlsxWriter Worksheet setting
       - value: appropriate value
       - see Worksheet methods at
         https://xlsxwriter.readthedocs.io/worksheet.html
       - supported settings:
           - autofilter:
             https://xlsxwriter.readthedocs.io/worksheet.html#autofilter
           - freeze_panes:
             https://xlsxwriter.readthedocs.io/worksheet.html#freeze_panes
   
   workbook default column settings:
       - used for each worksheet where worksheet column settings not specified in
         "worksheets"
       - list of dict
       - key: Worksheet.set_column() parameter keyword
       - value: Worksheet.set_column() parameter keyword value
           - cell_format value if given must be one of the names given in
             "cell_format_settings"
       - see https://xlsxwriter.readthedocs.io/worksheet.html#set_column
   
   workbook default row settings:
       - used for each worksheet where worksheet row settings not specified in
         "worksheets"
       - list of dict
       - key: Worksheet.set_row() parameter keyword
       - value: Worksheet.set_row() parameter keyword value
           - cell_format value if given must be one of the names given in
             "cell_format_settings"
       - see https://xlsxwriter.readthedocs.io/worksheet.html#set_row
   
   various worksheet-by-worksheet settings:
       - if a worksheet-by-worksheet setting key is provided, then its value
         overrides the workbook default for worksheets
       - list of dict
       - if an element is null or not provided, it is treated like {}
       - key:
           - name
           - worksheet_settings
           - column settings
           - row_settings
       - value: an appropriate value
   
   WORKBOOK CONFIGURATION EXAMPLES:
   
   A simple example with just workbook defaults set.
   
   {
       "workbook_options": {
           "strings_to_numbers": true
       },
       "cell_format_settings": {
           "general": {
               "text_wrap": true,
               "valign": "top",
               "num_format": "General"
           },
           "header": {
               "text_wrap": true,
               "valign": "bottom",
               "bold": true
           },
           "text": {
               "text_wrap": true,
               "valign": "top",
               "num_format": "@"
           },
           "number_p0": {
               "text_wrap": true,
               "valign": "top",
               "align": "right",
               "num_format": "0"
           },
           "datetime": {
               "text_wrap": true,
               "valign": "top",
               "align": "right",
               "num_format": "yyyy-mm-dd HH:MM:SS.000"
           }
       },
       "worksheet_settings": {
           "autofilter": [
               0,
               0,
               -1,
               3
           ],
           "freeze_panes": [
               1,
               0
           ]
       },
       "column_settings": [
           {
               "first_col": 0,
               "last_col": 0,
               "width": 40,
               "cell_format": "text"
           },
           {
               "first_col": 1,
               "last_col": 1,
               "width": 40,
               "cell_format": "text",
               "options": {
                   "hidden": true
               }
           },
           {
               "first_col": 2,
               "last_col": 2,
               "width": 20,
               "cell_format": "number_p0"
           },
           {
               "first_col": 3,
               "last_col": 3,
               "width": 30,
               "cell_format": "datetime"
           }
       ],
       "row_settings": [
           {
               "row": 0,
               "cell_format": "header"
           }
       ]
   }
   
   A more complicated example with a worksheet override set.
   
   {
       "workbook_options": {
           "strings_to_numbers": true
       },
       "cell_format_settings": {
           "general": {
               "text_wrap": true,
               "valign": "top",
               "num_format": "General"
           },
           "header": {
               "text_wrap": true,
               "valign": "bottom",
               "bold": true
           },
           "text": {
               "text_wrap": true,
               "valign": "top",
               "num_format": "@"
           },
           "number_p0": {
               "text_wrap": true,
               "valign": "top",
               "align": "right",
               "num_format": "0"
           },
           "datetime": {
               "text_wrap": true,
               "valign": "top",
               "align": "right",
               "num_format": "yyyy-mm-dd HH:MM:SS.000"
           }
       },
       "worksheet_settings": {
           "autofilter": [
               0,
               0,
               -1,
               3
           ],
           "freeze_panes": [
               1,
               0
           ]
       },
       "column_settings": [
           {
               "first_col": 0,
               "last_col": 0,
               "width": 40,
               "cell_format": "text"
           },
           {
               "first_col": 1,
               "last_col": 1,
               "width": 40,
               "cell_format": "text",
               "options": {
                   "hidden": true
               }
           },
           {
               "first_col": 2,
               "last_col": 2,
               "width": 20,
               "cell_format": "number_p0"
           },
           {
               "first_col": 3,
               "last_col": 3,
               "width": 30,
               "cell_format": "datetime"
           }
       ],
       "row_settings": [
           {
               "row": 0,
               "cell_format": "header"
           }
       ],
       "worksheets": [
           null,
           {
               "name": "foo",
               "worksheet_settings": {},
               "column_settings": [
                   {
                       "first_col": 0,
                       "last_col": 0,
                       "width": 20,
                       "cell_format": "text"
                   },
                   {
                       "first_col": 1,
                       "last_col": 1,
                       "width": 20,
                       "cell_format": "text"
                   },
                   {
                       "first_col": 2,
                       "last_col": 2,
                       "width": 20,
                       "cell_format": "number_p0"
                   },
                   {
                       "first_col": 3,
                       "last_col": 3,
                       "width": 30,
                       "cell_format": "datetime"
                   }
               ]
           }
       ]
   }
   
   COMMAND EXAMPLES:
   
       Read data from data.csv.  Write to standard output.
   
           xlsxf \
               data.csv
   
       Read data from data.csv.  Read configuration from data.config.json.  Write
       to standard output.
   
           xlsxf \
               --config data.config.json \
               data.csv
   
       Read data from data_1.csv and data_2.csv.  Read configuration from
       workbook.config.json.  Write to standard output.
   
           xlsxf \
               --config workbook.config.json \
               data_1.csv \
               data_2.csv
   
       Read data from data_1.csv and data_2.csv.  Read configuration from
       workbook.config.json.  Write to data.xlsx.
   
           xlsxf \
               --config workbook.config.json \
               --output data.xlsx \
               data_1.csv \
               data_2.csv
