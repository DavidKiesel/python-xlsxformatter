.. python-xlsxformatter documentation master file, created by
   sphinx-quickstart on Mon Jan 11 11:29:22 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to python-xlsxformatter's documentation!
================================================

Contents
========

.. toctree::
   :maxdepth: 2

   user_guide/index
   api
   integration_test_api

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
