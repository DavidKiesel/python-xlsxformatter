python-xlsxformatter
====================

# Introduction

Apply xlsx formatting.

Uses Python package [XlsxWriter](https://pypi.org/project/XlsxWriter).

For documentation see [Welcome to python-xlsxformatter's
documentation!](https://davidkiesel.bitbucket.io/python-xlsxformatter).
