#!/usr/bin/env python

#   -*- coding: utf-8 -*-

##############################################################################
# copyrights and license
#
# Copyright (c) 2021 David Harris Kiesel
#
# Licensed under the MIT License. See LICENSE in the project root for license
# information.
##############################################################################

##############################################################################
# file information
#
# https://pybuilder.io/documentation/manual
# https://gist.github.com/miebach/9752025

##############################################################################
# imports

from pybuilder.core import \
    Author, \
    init, \
    task, \
    use_plugin

##############################################################################
# plugins
# https://pybuilder.io/documentation/plugins

use_plugin('python.core')
use_plugin('python.install_dependencies')
use_plugin('python.flake8')
use_plugin('python.unittest')
use_plugin('python.integrationtest')
use_plugin('python.coverage')
use_plugin('python.distutils')

##############################################################################
# set project attributes
#
# at least some of these (e.g., version) need to be set as globals for
# PyBuilder to function properly
#
# PyBuilder automatically sets project attributes based on the globals

# sets setup.py variable name
name = 'suddenthought.xlsxformatter'

# sets setup.py variable version
# https://packaging.python.org/guides/distributing-packages-using-setuptools/#choosing-a-versioning-scheme
# https://www.python.org/dev/peps/pep-0440/
version = '1.0.0.dev'

#default_task = 'publish'
default_task = \
    [
        'clean',
        'analyze',
        'publish'
    ]

# sets setup.py variable description
summary = \
    'Apply xlsx formatting.'

# sets setup.py variable long_description
description = """
Apply xlsx formatting.
"""

# sets setup.py variable author and author_email
authors = \
    [
        Author(
            'David Harris Kiesel',
            'david@suddenthought.net'
        )
    ]

# sets setup.py variable maintainer and maintainer_email
maintainers = \
    [
        Author(
            'David Harris Kiesel',
            'david@suddenthought.net'
        )
    ]

# sets setup.py variable license
#
# https://choosealicense.com/
#
# https://spdx.org/licenses/
license = 'MIT'

# sets setup.py variable url
url = 'https://bitbucket.org/DavidKiesel/python-xlsxformatter'

# sets setup.py variable project_urls
urls = \
    {
            'Homepage': 'https://bitbucket.org/DavidKiesel/python-xlsxformatter',
            'Code': 'https://bitbucket.org/DavidKiesel/python-xlsxformatter',
            'Documentation': 'https://davidkiesel.bitbucket.io/python-xlsxformatter'
    }

# sets setup.py variable python_requires
requires_python = '>=3.8.6'

##############################################################################
# set project properties

@init
def set_properties(
    project
):
    ##########################################################################
    # install_dependencies plugin properties
    #
    # https://pybuilder.io/documentation/plugins

    project.depends_on_requirements(
        'requirements.txt'
    )

    project.depends_on_requirements(
        'requirements-dev.txt'
    )

    ##########################################################################
    # flake8 plugin properties
    #
    # https://pybuilder.io/documentation/plugins

    project.set_property(
        'flake8_include_test_sources',
        True
    )

    ##########################################################################
    # coverage plugin properties
    #
    # https://pybuilder.io/documentation/plugins

#    project.set_property(
#        'coverage_break_build',
#        False
#    )

    ##########################################################################
    # distutils plugin properties
    #
    # https://pybuilder.io/documentation/plugins

    # sets setup.py variable classifiers
    # https://pypi.org/classifiers/
    project.set_property(
        'distutils_classifiers',
        [
            'Development Status :: 4 - Beta',
            'Environment :: Console',
            'Intended Audience :: Developers',
            'Intended Audience :: End Users/Desktop',
            'License :: OSI Approved :: MIT License',
            'Operating System :: MacOS :: MacOS X',
            'Operating System :: Microsoft :: Windows',
            'Operating System :: POSIX',
            'Operating System :: Unix',
            'Programming Language :: Python',
            'Programming Language :: Python :: 3 :: Only',
            'Programming Language :: Python :: 3.8.6',
            'Topic :: Office/Business',
            'Topic :: Text Processing :: Markup :: XML',
            'Topic :: Utilities'
        ]
    )
